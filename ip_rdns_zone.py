#!/usr/bin/env python
# usage: don't. 
# fuck me, fuck this, fuck you.
# but most importantly? fuck python.

import sys, re
from netaddr import IPNetwork

if len(sys.argv) < 2:
    print("Pass me some friggin CIDR mate!")
    sys.exit(9)

net = IPNetwork(sys.argv[1])
if net.version == 6:
    # (bits/(bits of a nibble) * (2 bytes for representation of the nibble in address)) + constant size
    # => bits/4 * 2 + 9
    # => bits/2 + 9
    print(net.network.reverse_dns[-int(net.prefixlen/2 + 9):])
else:
    # legacyyy
    print(re.sub('^[0\.]+', '', net.network.reverse_dns))
