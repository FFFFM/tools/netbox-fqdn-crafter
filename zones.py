#!/usr/bin/env python3

import os
import sys
from datetime import datetime
from jinja2 import FileSystemLoader, Environment
from jinja2.ext import do
from netaddr import IPNetwork, valid_ipv4, valid_ipv6
from pynetbox import api
from pynetbox.core.api import Api


def is_ip_address(network):
    return valid_ipv6(network.split('/')[0]) or valid_ipv4(network.split('/')[0])


def render_template(file: str, data: dict, includes=None):
    if includes is None:
        includes = []

    for key, value in {
        'soa': 'a.ns.as64475.net',
        'mail': 'noc@wifi-frankfurt.de',
        'serial': datetime.utcnow().strftime('%Y%m%d%H'),
        'refresh': 60 * 60 * 3,
        'retry': 600,
        'expire': 60 * 60 * 24 * 7,
        'ttl': 600,
        'includes': ['ns.include'] + includes,
    }.items():
        data.setdefault(key, value)

    template = Environment(loader=FileSystemLoader(searchpath='./'), extensions=[do]).get_template(file)

    return template.render(**data)


def export_reverse(netbox: Api, parent_prefix: str):
    parent_prefix = {'net': IPNetwork(parent_prefix)}
    address_list = netbox.ipam.ip_addresses.filter(parent=str(parent_prefix['net']))
    child_prefixes = netbox.ipam.prefixes.filter(within_include=str(parent_prefix['net']))
    all_ips = netbox.ipam.ip_addresses.all()

    output = {}
    for prefix in child_prefixes:
        net = IPNetwork(prefix['prefix'])

        if net.cidr == parent_prefix['net'].cidr:
            parent_prefix['prefix'] = prefix
            continue

        output[prefix['prefix']] = {
            'prefix': prefix,
            'net': net,
        }

    for address in address_list:
        address = dict(address)
        net = IPNetwork(address['address'])

        is_primary = False
        if not address['dns_name'] and (address['assigned_object'] or address['nat_inside']):
            interface = address['assigned_object']
            address_id = address['id']

            if not interface and address['nat_inside']:
                address = list(filter(lambda e: e['id'] == address['nat_inside']['id'], all_ips))[0]
                address = dict(address)
                interface = address['assigned_object']

            if 'virtual_machine' in interface:
                vm = interface['virtual_machine']
                host = netbox.virtualization.virtual_machines.get(id=vm['id'])
            else:
                device = interface['device']
                host = netbox.dcim.devices.get(id=device['id'])

            address['dns_name'] = '%s.%s' % (interface['name'].replace('.', '-').lower(), host['name'])
            if (host['primary_ip4'] and host['primary_ip4']['id'] == address_id) \
                    or (host['primary_ip6'] and host['primary_ip6']['id'] == address_id):
                address['dns_name'] = host['name']
                is_primary = True

        if address['address'] not in output or is_primary or address['dns_name']:
            output[address['address']] = {
                'address': address,
                'net': net,
            }

    output = list(output.values())
    output.sort(key=lambda a: a['net'].first)

    zone = render_template('reverse-zone.j2', {
        'prefix': parent_prefix,
        'lines': output,
    })
    print(zone)


def export_zone(netbox: Api, domain: str):
    hardware = netbox.dcim.devices.filter(q=domain)
    vms = netbox.virtualization.virtual_machines.filter(q=domain)
    ips = netbox.ipam.ip_addresses.filter(assigned_to_interface=True)
    all_ips = netbox.ipam.ip_addresses.all()
    hosts = list(hardware) + list(vms)

    output = []
    for address in ips:
        interface = address['assigned_object']
        if 'virtual_machine' in interface:
            host = interface['virtual_machine']
        else:
            host = interface['device']

        if not host['name'].endswith('.' + domain) and not host['name'] == domain:  # Ends with domain
            continue

        host = list(filter(lambda e: e['name'] == host['name'], hosts))[0]
        hostname = '%s.%s' % (interface['name'].replace('.', '-').lower(), host['name'])
        if (host['primary_ip4'] and host['primary_ip4']['id'] == address['id']) \
                or (host['primary_ip6'] and host['primary_ip6']['id'] == address['id']):
            hostname = host['name']

        if address['nat_outside']:
            address = list(filter(lambda e: e['address'] == address['nat_outside']['address'], all_ips))[0]

        output.append({
            'ip': address,
            'name': hostname,
        })

    output.sort(key=lambda i: i['ip']['family']['value'] * -1)  # v6 first
    output.sort(key=lambda i: len(i['name']))  # Primary IPs first
    output.sort(key=lambda i: '.'.join(i['name'].split('.')[::-1]))  # Predictable sorting

    zone = render_template('zone.j2', {
        'zone': domain,
        'lines': output,
    }, [domain + '.include'])
    print(zone)


if __name__ == '__main__':
    if 'NETBOX_TOKEN' not in os.environ:
        print('NETBOX_TOKEN env not found')
        exit(2)

    arg = sys.argv[1]
    n = api('https://netbox.as64475.net', token=os.environ['NETBOX_TOKEN'])

    if is_ip_address(arg):
        export_reverse(n, arg)
        exit()

    export_zone(n, arg)
