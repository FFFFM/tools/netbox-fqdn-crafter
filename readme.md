# Create zone files from netbox
## Required Variables
* `$NETBOX_TOKEN`: Netbox API token

### CI
* `$ZONES` a list of zones to export
* `$DOMAINS` a list of domains to export
